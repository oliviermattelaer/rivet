BEGIN PLOT /H1_1996_I422230/d01-x01-y01
Title=Pn vs multiplicity $1 < \eta <2 $, $80 < W <115$ GeV
XLabel=$n$
YLabel=$P_n[\%]$
LegendXPos=0.45
END PLOT

BEGIN PLOT /H1_1996_I422230/d01-x01-y02
Title=Pn vs multiplicity $1 < \eta < 3 $, $80 < W < 115 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d01-x01-y03
Title=Pn vs multiplicity $1 < \eta < 4 $, $80 < W < 115 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
LegendYPos=0.6
LegendXPos=0.2
END PLOT

BEGIN PLOT /H1_1996_I422230/d01-x01-y04
Title=Pn vs multiplicity $1 < \eta < 5 $, $80 < W < 115 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d02-x01-y01
Title=Pn vs multiplicity $1 < \eta < 2 $, $115 < W < 150 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d02-x01-y02
Title=Pn vs multiplicity $1 < \eta < 3 $, $115 < W < 150 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d02-x01-y03
Title=Pn vs multiplicity $1 < \eta < 4 $, $115 < W < 150 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d02-x01-y04
Title=Pn vs multiplicity $1 < \eta < 5 $, $115 < W < 150 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d03-x01-y01
Title=Pn vs multiplicity $1 < \eta < 2 $, $150 < W < 185 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d03-x01-y02
Title=Pn vs multiplicity $1 < \eta < 3 $, $150 < W < 185 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d03-x01-y03
Title=Pn vs multiplicity $1 < \eta < 4 $, $150 < W < 185 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d03-x01-y04
Title=Pn vs multiplicity $1 < \eta < 5 $, $150 < W < 185 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d04-x01-y01
Title=Pn vs multiplicity $1 < \eta < 2 $, $185 < W < 220 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d04-x01-y02
Title=Pn vs multiplicity $1 < \eta < 3 $ , $185 < W < 220 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d04-x01-y03
Title=Pn vs multiplicity $1 < \eta < 4 $, $185 < W < 220 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d04-x01-y04
Title=Pn vs multiplicity $1 < \eta < 5 $, $185 < W < 220 $ GeV
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y01
LogY=0
Title=mean multiplicity vs $W$,   $ 0 < \eta$
XLabel=$W$ [GeV]
YLabel=$<n>$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y01
LogY=0
Title=mean multiplicity vs $W$,   $1<\eta<2$
XLabel=$W$ [GeV]
YLabel=$<n>$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y01
LogY=0
Title=mean multiplicity vs $W$,   $1<\eta<3$
XLabel=$W$ [GeV]
YLabel=$<n>$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y01
LogY=0
Title=mean multiplicity vs $W$,   $1<\eta<4$
XLabel=$W$ [GeV]
YLabel=$<n>$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y01
LogY=0
Title=mean multiplicity vs $W$,   $1<\eta<5$
XLabel=$W$ [GeV]
YLabel=$<n>$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y01
LogY=0
Title=mean multiplicity vs $W$,   $2<\eta<3$
XLabel=$W$ [GeV]
YLabel=$<n>$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y01
LogY=0
Title=mean multiplicity vs $W$,   $3<\eta<4$
XLabel=$W$ [GeV]
YLabel=$<n>$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y01
LogY=0
Title=mean multiplicity vs $W$,   $4<eta<5$
XLabel=$W$ [GeV]
YLabel=$<n>$
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y02
Title=D2 vs $W$,   $0< \eta$
XLabel=$W$ [GeV]
YLabel=$D_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y03
Title=D3 vs $W$,   $0 < \eta $
XLabel=$W$ [GeV]
YLabel=$D_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y04
Title=D4 vs $W$,   $0 < \eta $
XLabel=$W$ [GeV]
YLabel=$D_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y05
Title=C2 vs $W$,   $0 < \eta $
XLabel=$W$ [GeV]
YLabel=$C_2$
END PLOT
BEGIN PLOT /H1_1996_I422230/d05-x01-y06
Title=C3 vs $W$,   $0 < \eta $
XLabel=$W$ [GeV]
YLabel=$C_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y07
Title=C4 vs $W$,   $0 < \eta $
XLabel=$W$ [GeV]
YLabel=$C_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y08
LogY=0
YMin=0
YMax=2.00
Title=R2 vs $W$,   $0 < \eta $
XLabel=$W$ [GeV]
YLabel=$R_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y09
LogY=0
YMin=0
YMax=2.00
Title=R3 vs $W$,   $0 < \eta $
XLabel=$W$ [GeV]
YLabel=$R_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y02
Title=D2 vs $W$,   $1<\eta<2$
XLabel=$W$ [GeV]
YLabel=$D_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y03
Title=D3 vs $W$,   $1<\eta<2$
XLabel=$W$ [GeV]
YLabel=$D_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y04
Title=D4 vs $W$,   $1<\eta<2$
XLabel=$W$ [GeV]
YLabel=$D_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y05
Title=C2 vs $W$,   $1<\eta<2$
XLabel=$W$ [GeV]
YLabel=$C_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y06
Title=C3 vs $W$,   $1<\eta<2$
XLabel=$W$ [GeV]
YLabel=$C_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y07
Title=C4 vs $W$,   $1<\eta<2$
XLabel=$W$ [GeV]
YLabel=$C_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y08
LogY=0
YMin=0.5
YMax=2.00
Title=R2 vs $W$,   $1<\eta<2$
XLabel=$W$ [GeV]
YLabel=$R_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y09
LogY=0
YMin=1
YMax=2.00
Title=R3 vs $W$,   $1<\eta<2$
XLabel=$W$ [GeV]
YLabel=$R_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y10
LogY=0
YMin=0.01
YMax=1.00
Title=K3 vs $W$,   $1<\eta<2$
XLabel=$W$ [GeV]
YLabel=$K_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y02
Title=D2 vs $W$,   $1<\eta<3$
XLabel=$W$ [GeV]
YLabel=$D_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y03
Title=D3 vs $W$,   $1<\eta<3$
XLabel=$W$ [GeV]
YLabel=$D_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y04
Title=D4 vs $W$,   $1<\eta<4$
XLabel=$W$ [GeV]
YLabel=$D_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y05
Title=C2 vs $W$,   $1<\eta<3$
XLabel=$W$ [GeV]
YLabel=$C_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y06
Title=C3 vs $W$,   $1<\eta<3$
XLabel=$W$ [GeV]
YLabel=$C_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y07
Title=C4 vs $W$,   $1<\eta<3$
XLabel=$W$ [GeV]
YLabel=$C_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y08
LogY=0
YMin=0.5
YMax=2.00
Title=R2 vs $W$,   $1<\eta<3$
XLabel=$W$ [GeV]
YLabel=$R_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y09
LogY=0
YMin=0.5
YMax=2.00
Title=R3 vs $W$,   $1<\eta<3$
XLabel=$W$ [GeV]
YLabel=$R_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y10
LogY=0
YMin=0.0001
YMax=1.00
Title=K3 vs $W$,   $1<\eta<3$
XLabel=$W$ [GeV]
YLabel=$K_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y02
Title=D2 vs $W$,   $1<\eta<4$
XLabel=$W$ [GeV]
YLabel=$D_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y03
Title=D3 vs $W$,   $1<\eta<4$
XLabel=$W$ [GeV]
YLabel=$D_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y04
Title=D4 vs $W$,   $1<\eta<4$
XLabel=$W$ [GeV]
YLabel=$D_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y05
Title=C2 vs $W$,   $1<\eta<4$
XLabel=$W$ [GeV]
YLabel=$C_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y06
Title=C3 vs $W$,   $1<\eta<4$
XLabel=$W$ [GeV]
YLabel=$C_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y07
Title=C4 vs $W$,   $1<\eta<4$
XLabel=$W$ [GeV]
YLabel=$C_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y08
LogY=0
YMin=0
YMax=2.00
Title=R2 vs $W$,   $1<\eta<4$
XLabel=$W$ [GeV]
YLabel=$R_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y09
LogY=0
YMin=0
YMax=2.00
Title=R3 vs $W$,   $1<\eta<4$
XLabel=$W$ [GeV]
YLabel=$R_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y02
Title=D2 vs $W$,   $1<\eta<5$
XLabel=$W$ [GeV]
YLabel=$D_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y03
Title=D3 vs $W$,   $1<\eta<5$
XLabel=$W$ [GeV]
YLabel=$D_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y04
Title=D4 vs $W$,   $1<\eta<5$
XLabel=$W$ [GeV]
YLabel=$D_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y05
Title=C2 vs $W$,   $1<\eta<5$
XLabel=$W$ [GeV]
YLabel=$C_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y06
Title=C3 vs $W$,   $1<\eta<5$
XLabel=$W$ [GeV]
YLabel=$C_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y07
Title=C4 vs $W$,   $1<\eta<5$
XLabel=$W$ [GeV]
YLabel=$C_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y08
LogY=0
YMin=0
YMax=2.00
Title=R2 vs $W$,   $1<\eta<5$
XLabel=$W$ [GeV]
YLabel=$R_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y09
LogY=0
YMin=0
YMax=2.00
Title=R3 vs $W$,   $1<\eta<5$
XLabel=$W$ [GeV]
YLabel=$R_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y02
Title=D2 vs $W$,   $2<\eta<3$
XLabel=$W$ [GeV]
YLabel=$D_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y03
Title=D3 vs $W$,   $2<\eta<3$
XLabel=$W$ [GeV]
YLabel=$D_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y04
Title=D4 vs $W$,   $2<\eta<3$
XLabel=$W$ [GeV]
YLabel=$D_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y05
Title=C2 vs $W$,   $2<\eta<3$
XLabel=$W$ [GeV]
YLabel=$C_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y06
Title=C3 vs $W$,   $2<\eta<3$
XLabel=$W$ [GeV]
YLabel=$C_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y07
Title=C4 vs $W$,   $2<\eta<3$
XLabel=$W$ [GeV]
YLabel=$C_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y08
LogY=0
YMin=0
YMax=2.00
Title=R2 vs $W$,   $2<\eta<3$
XLabel=$W$ [GeV]
YLabel=$R_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y09
LogY=0
YMin=1
YMax=3.5
Title=R3 vs $W$,   $2<\eta<3$
XLabel=$W$ [GeV]
YLabel=$R_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y10
LogY=0
YMin=-0.4
YMax=0.6
Title=K3 vs $W$,   $2<\eta<3$
XLabel=$W$ [GeV]
YLabel=$K_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y02
Title=D2 vs $W$,   $3<\eta<4$
XLabel=$W$ [GeV]
YLabel=$D_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y03
Title=D3 vs $W$,   $3<\eta<4$
XLabel=$W$ [GeV]
YLabel=$D_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y04
Title=D4 vs $W$,   $3<\eta<4$
XLabel=$W$ [GeV]
YLabel=$D_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y05
Title=C2 vs $W$,   $3<\eta<4$
XLabel=$W$ [GeV]
YLabel=$C_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y06
Title=C3 vs $W$,   $3<\eta<4$
XLabel=$W$ [GeV]
YLabel=$C_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y07
Title=C4 vs $W$,   $3<\eta<4$
XLabel=$W$ [GeV]
YLabel=$C_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y08
LogY=0
Title=R2 vs $W$,   $3<\eta<4$
XLabel=$W$ [GeV]
YLabel=$R_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y09
LogY=0
Title=R3 vs $W$,   $3<\eta<4$
XLabel=$W$ [GeV]
YLabel=$R_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y10
LogY=0
Title=K3 vs $W$,   $3<\eta<4$
XLabel=$W$ [GeV]
YLabel=$K_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y02
Title=D2 vs $W$,   $4<\eta<5$
XLabel=$W$ [GeV]
YLabel=$D_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y03
Title=D3 vs $W$,   $4<\eta<5$
XLabel=$W$ [GeV]
YLabel=$D_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y04
Title=D4 vs $W$,   $4<\eta<5$
XLabel=$W$ [GeV]
YLabel=$D_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y05
Title=C2 vs $W$,   $4<\eta<5$
XLabel=$W$ [GeV]
YLabel=$C_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y06
Title=C3 vs $W$,   $4<\eta<5$
XLabel=$W$ [GeV]
YLabel=$C_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y07
Title=C4 vs $W$,   $4<\eta<5$
XLabel=$W$ [GeV]
YLabel=$C_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y08
LogY=0
Title=R2 vs $W$,   $4<\eta<5$
XLabel=$W$ [GeV]
YLabel=$R_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y09
LogY=0
Title=R3 vs $W$,   $4<\eta<5$
XLabel=$W$ [GeV]
YLabel=$R_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y10
LogY=0
YMin=-0.15
Title=K3 vs $W$, ,   $4<\eta<5$
XLabel=$W$ [GeV]
YLabel=$K_3$
END PLOT
