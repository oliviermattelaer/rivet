# BEGIN PLOT /CMS_2021_I1920187_DIJET/d02-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d04-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d06-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d08-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d10-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d12-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d14-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d16-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d18-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d155-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d156-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d157-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d158-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d159-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d160-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d161-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d162-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d163-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d164-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d165-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d179-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d180-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d181-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d182-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d183-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d184-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d185-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d186-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d187-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d188-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d189-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d190-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d191-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d209-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d210-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d211-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d212-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d213-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d214-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d215-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d216-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d217-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d218-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d219-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d220-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d234-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d235-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d236-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d237-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d238-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d239-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d240-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d241-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d242-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d243-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d244-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d245-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d246-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d265-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d266-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d267-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d268-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d269-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d270-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d271-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d272-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d273-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d274-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d275-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d276-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d277-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d291-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d292-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d293-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d294-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d295-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d296-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d297-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d298-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d299-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d300-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d301-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d302-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d303-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d322-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d323-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d324-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d325-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d326-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d327-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d328-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d329-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d330-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d331-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d332-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d333-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d334-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d348-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d349-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d350-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d351-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d352-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d353-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d354-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d355-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d356-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d357-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d358-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d359-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d360-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d379-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d380-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d381-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d382-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d383-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d384-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d385-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d386-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d387-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d388-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d389-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d390-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d391-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d405-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d406-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d407-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d408-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d409-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d410-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d411-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d412-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d413-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d414-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d415-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d416-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d417-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d436-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d437-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d438-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d439-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d440-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d441-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d442-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d443-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d444-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d445-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d446-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d447-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d448-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d462-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d463-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d464-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d465-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d466-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d467-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d468-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d469-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d470-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d471-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d472-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d473-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d474-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d492-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d493-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d494-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d495-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d496-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d497-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d498-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d499-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d500-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d501-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d502-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d503-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d517-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d518-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d519-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d520-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d521-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d522-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d523-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d524-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d525-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d526-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d527-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d528-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d529-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d547-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d548-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d549-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d550-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d551-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d552-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d553-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d554-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d555-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d556-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d557-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d558-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d572-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d573-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d574-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d575-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d576-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d577-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d578-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d579-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d580-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d581-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d582-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d583-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d584-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d602-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d603-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d604-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d605-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d606-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d607-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d608-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d609-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d610-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d611-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d612-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d613-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d627-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d628-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d629-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d630-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d631-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d632-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d633-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d634-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d635-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d636-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d637-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d638-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d639-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d657-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d658-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d659-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d660-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d661-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d662-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d663-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d664-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d665-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d666-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d667-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d668-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d682-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d683-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d684-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d685-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d686-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d687-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d688-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d689-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d690-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d691-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d692-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d693-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d694-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d712-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d713-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d714-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d715-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d716-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d717-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d718-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d719-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d720-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d721-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d722-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d723-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d737-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d738-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d739-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d740-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d741-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d742-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d743-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d744-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d745-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d746-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d747-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d748-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d749-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d768-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d769-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d770-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d771-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d772-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d773-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d774-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d775-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d776-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d777-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d778-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d779-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d780-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d794-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d795-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d796-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d797-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d798-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d799-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d800-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d801-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d802-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d803-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d804-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d805-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d806-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d825-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d826-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d827-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d828-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d829-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d830-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d831-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d832-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d833-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d834-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d835-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d836-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d837-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d851-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d852-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d853-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d854-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d855-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d856-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d857-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d858-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d859-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d860-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d861-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d862-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d863-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d882-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d883-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d884-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d885-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d886-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d887-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d888-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d889-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d890-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d891-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d892-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d893-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d894-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d908-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d909-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d910-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d911-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d912-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d913-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d914-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d915-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d916-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d917-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d918-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d919-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d920-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d939-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d940-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d941-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d942-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d943-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d944-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d945-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d946-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d947-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d948-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d949-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d950-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d951-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d965-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d966-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d967-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d968-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d969-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d970-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d971-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d972-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d973-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d974-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d975-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d976-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d977-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d996-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d997-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d998-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d999-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1000-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1001-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1002-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1003-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1004-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1005-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1006-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1007-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1008-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1022-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1023-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1024-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1025-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1026-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1027-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1028-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1029-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1030-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1031-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1032-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1033-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1034-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1053-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1054-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1055-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1056-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1057-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1058-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1059-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1060-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1061-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1062-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1063-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1064-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1065-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1079-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1080-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1081-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1082-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1083-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1084-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1085-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1086-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1087-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1088-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1089-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1090-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1091-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1110-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1111-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1112-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1113-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1114-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1115-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1116-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1117-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1118-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1119-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1120-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1121-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1122-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1136-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1137-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1138-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1139-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1140-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1141-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1142-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1143-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1144-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1145-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1146-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1147-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1148-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1167-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1168-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1169-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1170-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1171-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1172-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1173-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1174-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1175-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1176-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1177-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1178-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1179-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1193-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1194-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1195-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1196-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1197-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1198-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1199-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1200-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1201-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1202-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1203-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1204-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1205-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1224-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1225-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1226-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1227-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1228-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1229-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1230-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1231-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1232-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1233-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1234-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1235-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1236-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1250-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1251-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1252-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1253-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1254-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1255-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1256-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1257-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1258-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1259-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1260-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1261-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1262-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1271-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1272-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1273-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1274-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1275-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1276-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1277-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1278-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1279-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1280-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1281-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1282-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1283-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1284-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1285-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1286-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1287-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1288-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1289-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1290-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1291-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1292-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1293-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1294-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1295-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1314-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1315-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1316-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1317-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1318-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1319-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1320-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1321-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1322-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1323-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1324-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1325-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1326-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1327-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1328-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1329-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1330-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1331-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1332-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1333-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1334-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1335-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1336-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1337-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1338-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1339-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1358-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1359-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1360-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1361-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1362-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1363-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1364-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1365-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1366-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1367-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1368-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1369-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1370-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1371-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1372-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1373-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1374-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1375-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1376-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1377-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1378-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1379-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1380-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1381-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1382-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1383-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1402-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1403-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1404-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1405-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1406-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1407-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1408-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1409-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1410-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1411-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1412-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1413-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1414-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1415-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1416-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1417-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1418-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1419-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1420-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1421-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1422-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1423-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1424-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1425-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1426-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1427-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1446-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1447-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1448-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1449-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1450-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1451-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1452-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1453-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1454-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1455-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1456-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1457-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1458-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1459-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1460-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1461-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1462-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1463-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1464-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1465-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1466-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1467-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1468-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1469-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1470-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1471-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1490-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1491-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1492-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1493-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1494-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1495-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1496-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1497-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1498-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1499-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1500-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1501-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1502-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1503-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1504-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1505-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1506-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1507-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1508-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1509-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1510-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1511-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1512-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1513-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1514-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1515-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1525-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1526-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1527-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1528-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1529-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1530-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1531-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1532-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1533-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1534-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1535-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1536-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1537-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1538-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1539-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1540-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1541-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1542-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1543-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1544-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1545-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1546-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1547-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1548-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1549-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1550-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1560-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1561-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1562-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1563-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1564-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1565-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1566-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1567-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1568-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1569-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1570-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1571-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1572-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1573-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1574-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1575-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1576-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1577-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1578-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1579-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1580-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1581-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1582-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1583-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1584-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1585-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1595-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1596-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1597-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1598-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1599-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1600-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1601-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1602-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1603-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1604-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1605-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1606-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1607-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1608-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1609-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1610-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1611-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1612-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1613-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1614-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1615-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1616-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1617-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1618-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1619-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1620-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1630-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1631-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1632-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1633-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1634-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1635-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1636-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1637-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1638-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1639-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1640-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1641-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1642-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1643-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1644-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1645-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1646-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1647-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1648-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1649-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1650-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1651-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1652-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1653-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1654-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1655-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1674-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1675-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1676-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1677-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1678-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1679-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1680-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1681-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1682-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1683-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1684-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1685-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1686-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1687-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1688-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1689-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1690-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1691-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1692-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1693-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1694-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1695-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1696-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1697-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1698-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1699-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1718-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1719-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1720-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1721-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1722-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1723-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1724-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1725-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1726-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1727-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1728-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1729-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1730-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1731-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1732-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1733-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1734-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1735-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1736-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1737-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1738-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1739-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1740-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1741-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1742-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1743-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1762-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1763-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1764-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1765-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1766-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1767-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1768-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1769-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1770-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1771-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1772-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1773-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1774-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1775-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1776-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1777-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1778-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1779-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1780-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1781-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1782-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1783-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1784-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1785-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1786-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1787-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1806-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1807-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1808-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1809-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1810-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1811-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1812-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1813-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1814-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1815-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1816-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1817-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1818-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1819-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1820-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1821-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1822-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1823-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1824-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1825-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1826-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1827-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1828-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1829-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1830-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1831-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1850-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1851-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1852-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1853-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1854-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1855-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1856-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1857-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1858-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1859-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1860-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1861-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1862-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1863-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1864-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1865-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1866-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1867-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1868-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1869-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1870-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1871-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1872-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1873-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1874-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1875-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1894-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1895-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1896-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1897-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1898-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1899-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1900-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1901-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1902-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1903-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1904-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1905-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1906-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1907-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1908-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1909-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1910-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1911-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1912-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1913-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1914-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1915-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1916-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1917-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1918-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1919-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1938-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1939-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1940-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1941-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1942-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1943-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1944-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1945-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1946-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1947-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1948-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1949-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1950-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1951-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1952-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1953-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1954-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1955-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1956-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1957-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1958-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1959-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1960-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1961-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1962-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1963-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1982-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1983-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1984-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1985-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1986-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1987-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1988-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1989-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1990-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1991-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1992-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1993-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1994-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1995-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1996-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1997-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1998-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d1999-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2000-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2001-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2002-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2003-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2004-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2005-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2006-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2007-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2026-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2027-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2028-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2029-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2030-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2031-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2032-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2033-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2034-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2035-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2036-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2037-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2038-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2039-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2040-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2041-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2042-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2043-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2044-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2045-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2046-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2047-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2048-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2049-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2050-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2051-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2070-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2071-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2072-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2073-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2074-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2075-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2076-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2077-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2078-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2079-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2080-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2081-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2082-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2083-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2084-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2085-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2086-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2087-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2088-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2089-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2090-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2091-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2092-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2093-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2094-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_DIJET/d2095-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT
